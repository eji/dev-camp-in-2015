# 以下探した宿一覧

## サニーヴィレッジ

  - 場所: 軽井沢
  - URL: http://cdn.e-monkey.jp/
  - TEL: 0267-42-3882

## ネオオリエンタルリゾート八ヶ岳高原コテージ
  - 場所: 八ヶ岳
  - URL: http://yatsugatake.izumigo.co.jp/
  - tel: 0551-38-2336

# 候補

# 明治屋旅館

  - 場所: 軽井沢
  - tel: 0267-32-2028
  - URL: http://meijiyaryokan.com/cuisine/
  - 料金:
    - ２泊: ※要確認（プラン大杉）

  - 移動
    - ルート: 東京駅 --[あさま]--> 軽井沢駅 --[しなの鉄道]--> 御代田駅 --[徒歩]--> 宿
    - 乗り物: 新幹線+鈍行+徒歩
    - 時間: 約2時間
    - 料金: 約6200円（往復12400円）

  - 設備:
    - 無線LAN

## 以下要調査

  - 湯河原温泉　懐石旅庵　阿しか里
    - 場所: 湯河原
    - tel: 0465-62-4151
    - URL: http://travel.rakuten.co.jp/HOTEL/67213/67213.html?cid=tr_af_1632&scid=af_pc_link_txt&sc2id=37695979

  - 強羅温泉　桐谷　箱根荘
    - 場所: 箱根
    - tel: 0460-82-2246
    - URL: http://travel.rakuten.co.jp/HOTEL/18497/18497.html?cid=tr_af_1632&scid=af_pc_link_txt&sc2id=37695979

  - 湯河原温泉　加満田
    - 場所: 湯河原
    - tel: 0465-62-2151
    - URL: http://travel.rakuten.co.jp/HOTEL/79359/79359.html?cid=tr_af_1632&scid=af_pc_link_txt&sc2id=37695979

  - 箱根　金乃竹
    - 場所: 箱根
    - tel: 0460-84-3939
    - URL: http://travel.rakuten.co.jp/HOTEL/80586/80586.html?cid=tr_af_1632&scid=af_pc_link_txt&sc2id=37695979

  - 箱根湯本温泉　雉子亭　豊栄荘
    - 場所: 箱根
    - tel: 0460-85-5763
    - URL: http://travel.rakuten.co.jp/HOTEL/19256/19256.html?cid=tr_af_1632&scid=af_pc_link_txt&sc2id=37695979

  - かみのやま温泉
    - 場所: 山形
    - tel: 023-672-2511
    - URL: http://travel.rakuten.co.jp/HOTEL/19518/19518.html

  - 富士屋ホテル
    - 場所: 箱根
    - tel: 0460-82-2211
    - URL: http://www.jtb.co.jp/kokunai_htl/list/A04/14/1406/140605/4309001/#htl_contents

  - まにわ
    - 場所: 水上
    - tel: 0278-72-2445
    - URL: http://www.minsyukumaniwa.com/info/info.html

  - 山中湖フォレストコテージ
    - 場所: 山中湖
    - URL: http://www.odakyu-forest.com/top.html
    - tel: 0555-62-2171

# 既に満席状態

## はじめのいっぽ 炭儀焼きの宿
  - 場所: 群馬
  - URL: http://ippo.jp/
   - URL: http://www.pension-mokuba.com/shisetu/index.html
  - 料金:
    - ２泊: 15400円/人
  - 移動
    - ルート: 東京駅 --[やまびこ]--> 宇都宮駅 --[日光線]--> 日光駅 --[車]--> 宿
    - 乗り物: 新幹線+鈍行+車
    - 時間: 約３時間（東京 -> 日光）
    - 料金: 約5580円（往復11820円）
  - 設備
    - 無線LAN
    - ダイニングルームのテーブルを使える
    - 夜間作業可

## きのくにや
  - 場所: 箱根
  - URL: http://bihadanoyu-kinokuniya.com/

## 望雲
  - 場所: 草津
  - URL: http://www.hotelboun.com/

## 伊豆高原温泉　客室露天風呂付リゾートホテル　コルテラルゴ伊豆高原
  - 場所: 伊豆
  - URL: http://travel.rakuten.co.jp/HOTEL/38761/38761_std.html


## 伊豆大島 ホテル&リゾート マシオ
  - 場所: 伊豆大島
  - URL: http://www.mashio.com/
  - 料金:
    - ２泊: 40000円/人

  - 移動
    - 乗り物: 船
    - 時間: たぶん３時間（東京 -> 大島）
    - 料金: たぶん0円（２泊すれば6000円補助が出て、実質0円）

  - 設備
    - 無線LAN
    - テーブル席使えるのか不明


## 大平台温泉　たきい旅館
  - 場所: 箱根
  - URL: http://travel.rakuten.co.jp/HOTEL/19358/19358.html?cid=tr_af_1632&scid=af_pc_link_txt&sc2id=37695979

## 近江屋旅館
  - 場所: 箱根
  - URL: http://travel.rakuten.co.jp/HOTEL/129573/129573.html?cid=tr_af_1632&scid=af_pc_link_txt&sc2id=37695979


## 箱根温泉　鶴井の宿　紫雲荘
  - 場所: 箱根
  - URL: http://travel.rakuten.co.jp/HOTEL/29488/29488.html

## 河原温泉　おやど瑞月
  - 場所: 湯河原
  - URL: http://travel.rakuten.co.jp/HOTEL/52828/52828.html?cid=tr_af_1632&scid=af_pc_link_txt&sc2id=37695979


## 谷川くるみ村のペンション木馬
  - 場所: 群馬
  - URL: http://www.pension-mokuba.com/shisetu/index.html
  - 料金: 0278-72-6152
    - ２泊: 16000円/人
  - アクセス:
    - ルート: 東京駅 --[上越新幹線]--> 上毛高原駅 --[路線バス] --> 水上駅 --[車|送迎]-->宿
    - 乗り物: 新幹線+バス+車
    - 時間: 約1.5時間（東京 -> 上毛高原駅）
    - 料金: 5910円（往復11820円）

  - 設備
    - 無線LAN
    - ダイニングルームのテーブルを使える
    - 夜間作業可

## ホテル軽井沢1130

  - 場所: 軽井沢
  - tel: 0279-86-6111
  - URL: http://www.karuizawaclub.co.jp/hotel1130/
  - 料金:
    - ２泊: ※要確認（プラン大杉）

  - 移動
    - ルート: 東京駅 --[あさま]--> 軽井沢駅 --[送迎バス]--> 宿
    - 乗り物: 新幹線+バス
    - 時間: 約2時間
    - 料金: 約5900円（往復11800円）

  - 設備
    - 無線LAN（※プレミアムルームのみ）

## ホーム  ラフォーレホテルズ&リゾーツ

ここITS効くぞ。。。すげーよさそう

  - 場所: 軽井沢
  - tel: 0267-44-4489
  - URL: http://www.laforet.co.jp/lfhotels/krz/conference.html
  - 料金:
    - ２泊: ※要確認（プラン大杉）

  - 移動
    - ルート: 東京駅 --[あさま]--> 軽井沢駅 --[しなの鉄道]--> 中軽井沢駅 --[タクシー]--> 宿
    - 乗り物: 新幹線+鈍行+タクシー
    - 時間: 約2時間
    - 料金: 約7000円（往復14000円）

  - 設備:
    - 無線LAN
    - 会議室
    - プロジェクタ

## ホテルアンビエント蓼科コテージ

  - 場所: 蓼科
  - URL: http://tateshina.izumigo.co.jp/
  - TEL: 0267-55-7711
