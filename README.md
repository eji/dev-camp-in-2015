![https---www.pakutaso.com-assets_c-2015-06-ERY82_separate20131225-thumb-1000xauto-16573.jpg](https://bitbucket.org/repo/yzx9e4/images/2766351061-https---www.pakutaso.com-assets_c-2015-06-ERY82_separate20131225-thumb-1000xauto-16573.jpg)

# 目的

普段とは違う場所でプログラミングを楽しむこと

# 連絡手段

  - Slackもしくは電話

# スケジュール

 - 2015/9 or 2015/10頃

# 持っていくもの

  - PC
  - 電源タップ
  - 小型プロジェクタ
  - ネットワーク環境
  - 服

  - 海外の場合　
    - パスポート
    - 飛行機のチケット
    - 入国ビザ
    - 電源変換アダプタ
    - 旅行保険
    - クレジットカード